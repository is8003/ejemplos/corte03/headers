#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "hipico.h"

void generarCarreras(struct Jinete part[], unsigned jins, unsigned cars){
	for (size_t i = 0; i < jins; i++){
		for (size_t j = 0; j < cars; j++){
			unsigned t;
			bool encontrado;

			do{
				encontrado = false;
				t = (rand() % 100) + 100;
				for (size_t k = 0; k < i; k++){
					if (part[k].tiempos[j] == t){
						encontrado = true;
						break;
					}
				}
			}while(encontrado);

			part[i].tiempos[j] = t;
		}
	}
}

void imprimirCarreras(const struct Jinete part[], unsigned pods[][3],
		unsigned jins, unsigned cars){
	puts(" Carrera |        1ro      |        2do      |        3ro      |");
	puts("     No. | Nombre  | T [s] | Nombre  | T [s] | Nombre  | T [s] |");

	for (unsigned c = 0; c < cars; c++){
		unsigned idJinPodio[JINETES];

		for (size_t i = 0; i < jins; i++){
			idJinPodio[i] = i;
		}

		for (size_t i = 0; i < 3; i++){
			unsigned posMin = i;
			for (size_t j = i + 1; j < jins; j++){
				if (part[idJinPodio[j]].tiempos[c] 
						< part[idJinPodio[posMin]].tiempos[c]){
					posMin = j;
				}
			}

			if (posMin != i){
				unsigned auxId = idJinPodio[i];
				idJinPodio[i] = idJinPodio[posMin];
				idJinPodio[posMin] = auxId;
			}
		}
		pods[c][0] = idJinPodio[0];
		pods[c][1] = idJinPodio[1];
		pods[c][2] = idJinPodio[2];

		printf("%8d |", c + 1);
		printf("%8s |", part[idJinPodio[0]].nombre);
		printf("%6u |", part[idJinPodio[0]].tiempos[c]);
		printf("%8s |", part[idJinPodio[1]].nombre);
		printf("%6u |", part[idJinPodio[1]].tiempos[c]);
		printf("%8s |", part[idJinPodio[2]].nombre);
		printf("%6u |\n", part[idJinPodio[2]].tiempos[c]);
	}
}

const char * ganador(const struct Jinete part[], unsigned pods[][3],
		unsigned jins, unsigned cars){
	unsigned victorias[JINETES] = {0};

	for (size_t i = 0; i < cars; i ++){
		victorias[pods[i][0]]++;
	}

	unsigned maxVic = 0;
	unsigned idGan;

	for (size_t i = 0; i < jins; i++){
		if (victorias[i] > maxVic){
			maxVic = victorias[i];
			idGan = i;
		}
	}
	return part[idGan].nombre;
}


void calcularPuntos(struct Jinete part[], unsigned pods[][3],
		unsigned jins, unsigned cars){
	for (size_t i = 0; i < jins; i++){
		part[i].puntos = 0;
	}

	for (size_t i = 0; i < cars; i++){

		part[pods[i][0]].puntos += 5;

		if (part[pods[i][1]].tiempos[i] - part[pods[i][0]].tiempos[i] > 5){
			part[pods[i][0]].puntos++;
		}

		part[pods[i][1]].puntos += 3;
		part[pods[i][2]].puntos++;
	}
}

void reportarTorneo(const struct Jinete part[], unsigned jins){
	unsigned idJin[JINETES];

	for (size_t i = 0; i < jins; i++){
		idJin[i] = i;
	}

	for (size_t i = 0; i < jins - 1; i++){
		unsigned posMax = i;
		for (size_t j = i + 1; j < jins; j++){
			if (part[idJin[j]].puntos > part[idJin[posMax]].puntos){
				posMax = j;
			}
		}

		if (posMax != i){
			unsigned auxId = idJin[i];
			idJin[i] = idJin[posMax];
			idJin[posMax] = auxId;
		}
	}

	puts("Pos. |  Nombre | Puntos |");
	for (unsigned i = 0; i < jins; i++){
		printf("%4u | %7s | %6u |\n", i+1, part[idJin[i]].nombre, part[idJin[i]].puntos);
	}
}
