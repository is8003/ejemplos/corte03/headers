#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include "hipico.h"

int main(void){
	struct Jinete participantes[JINETES];
	unsigned podios[CARRERAS][3];

	participantes[0].id = 0;
	strcpy(participantes[0].nombre, "JuanR");
	participantes[1].id = 1;
	strcpy(participantes[1].nombre, "MiguelP");
	participantes[2].id = 2;
	strcpy(participantes[2].nombre, "AnaM");
	participantes[3].id = 3;
	strcpy(participantes[3].nombre, "LuisG");
	participantes[4].id = 4;
	strcpy(participantes[4].nombre, "PedroJ");
	participantes[5].id = 5;
	strcpy(participantes[5].nombre, "LinaQ");
	participantes[6].id = 6;
	strcpy(participantes[6].nombre, "CarlosS");


	srand(time(NULL));	// Inicializa generador de números.

	generarCarreras(participantes, JINETES, CARRERAS);

	imprimirCarreras(participantes, podios, JINETES, CARRERAS);

	printf("\nJinete con más victorias: %s\n\n",
			ganador(participantes, podios, JINETES, CARRERAS));

	calcularPuntos(participantes, podios, JINETES, CARRERAS);

	reportarTorneo(participantes, JINETES);
}
