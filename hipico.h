#ifndef HIPICO_H
#define HIPICO_H

#define JINETES 7
#define CARRERAS 10

// Estructura
struct Jinete {
	unsigned id;	// Identificador del jinete.
	char nombre[20]; // Nombre del jinete.
	unsigned tiempos[CARRERAS]; // Arreglo con tiempos en segundos.
	unsigned puntos; // Total de puntaje obtenido durante el torneo.
};

// Prototipos
void generarCarreras(struct Jinete part[], unsigned jins, unsigned cars);
void imprimirCarreras(const struct Jinete part[], unsigned pods[][3],
		unsigned jins, unsigned cars);
const char * ganador(const struct Jinete part[], unsigned pods[][3],
		unsigned jins, unsigned cars);
void calcularPuntos(struct Jinete part[], unsigned pods[][3],
		unsigned jins, unsigned cars);
void reportarTorneo(const struct Jinete part[], unsigned jins);

#endif
