# Enunciado

En un torneo hípico se registra la información usando una estructura por jinete
con los siguientes miembros:

- Identificador del jinete.
- Nombre del jinete.
- Arreglo con tiempos en segundos de cada una de las carreras que participó.
- Total de puntaje obtenido durante el torneo.

Todos los jinetes deben estar contenidos en un arreglo de participantes.
Ejemplo:

```text
   0   |    1    |   2  |   3   |   4    |   5   |    6    | 
 JuanR | MiguelP | AnaM | LuisG | PedroJ | LinaQ | CarlosS |
  ...  |   ...   |  ... |  ...  |  ...   |  ...  |   ...   | 
```


Teniendo en cuenta la información disponible, lo que se solicita es:

1. Defina la estructura que contenga la información relativa de un `jinete`.

1. Inicialice el arreglo de `participantes` con identificadores y nombres.

1. Modifique la función `generarCarreras` para que asigne tiempos aleatorios
   entre 100 y 200 segundos por cada carrera de un jinete . Esta función debe
   recibir como parámetro la estructura de un jinete y la cantidad de carreras.

1. La función `imprimirCarreras` debe listar en pantalla el podio de
   todas las carreras. El listado debe incluir, el número de carrera, el nombre
   del jinete, el tiempo en segundos y la posición. Ejemplo:

    ```text
    Carrera |      Pos. 1     |      Pos. 2     |      Pos. 3     |
        No. | Nombre  | T [s] | Nombre  | T [s] | Nombre  | T [s] |
          1 |   LinaQ |   158 | MiguelP |   173 |   LuisG |   200 |
          2 | CarlosS |   100 | MiguelP |   167 |   LinaQ |   169 |
          3 |   LuisG |   156 | CarlosS |   158 |    AnaM |   195 |
          4 |  PedroJ |   155 |   LinaQ |   162 | MiguelP |   173 |
          5 |   LuisG |   183 |   JuanR |   197 | MiguelP |   199 |
          6 |   JuanR |   107 |   LinaQ |   116 | CarlosS |   158 |
          7 |    AnaM |   105 |   LuisG |   153 |   JuanR |   197 |
          8 |  PedroJ |   138 |    AnaM |   145 |   LinaQ |   176 |
          9 |   LuisG |   150 | MiguelP |   153 |   LinaQ |   175 |
         10 |  PedroJ |   159 |   LuisG |   175 | MiguelP |   183 |
    ```

1. Modifique la función `ganador`, esta debe retornar el nombre del jinete que
   ganó más carreras en el torneo.

1. La función `calcularPuntos` debe consignar la puntuación que se da a los
   jinetes participantes. Tenga en cuenta que algún jinete podría no tener
   puntos. El esquema de puntuación es el siguiente: 

   1. Por cada carrera ganada se le otorgan 5 puntos. Si la diferencia de
   tiempo entre el ganador y el segundo puesto es mayor a 5 segundos se le
   otorga un punto adicional al ganador. 

   1. Por cada vez que ocupa el segundo puesto se le otorgan 3 puntos.

   1. Por cada vez que ocupa el tercer puesto se le otorgan 1 puntos.

1. Modifique la función `reportarTorneo` para que produzca el reporte del
   torneo. El reporte debe incluir el nombre del jinete y los puntos obtenidos,
   ordenado de manera descendente teniendo como criterio los puntos. Ejemplo:

    ```text
    Pos. |  Nombre | Puntos |
       1 |   LuisG |     22 |
       2 |  PedroJ |     18 |
       3 |   LinaQ |     12 |
       4 |   JuanR |      9 |
       5 | MiguelP |      9 |
       6 |    AnaM |      9 |
       7 | CarlosS |      9 |
    ```

# Refinamiento

## Función: `generarCarreras`

Asigna tiempos aleatorios entre 100 y 199 segundos en arreglo de tamaño igual a
cantidad de carreras por cada jinete.

### Entradas

- `part`: arreglo con datos de jinetes.
- `jins`: cantidad de jinetes.
- `cars`: cantidad de carreras.

### Salidas

- Ninguna.

### Pseudo-código

1. Para `i` desde `0`; hasta `i` menor igual a cantidad de jinetes `jins`;
   incremento `i` en `1`:
   1. Para `j` desde `0`; hasta `j` menor igual a cantidad de carreras `cars`;
      incremento `j` en `1`:
      1. Declaro `t` para almacenar temporalmente tiempo aleatorio.
      1. Declaro `encontrado` como bandera.
      1. Hacer:
         1. Asigno falso a `encontrado`.
         1. Genero valor aleatorio entre `100` y `199` y asigno a `t`.
         1. Para `k` desde `0`; hasta `k` menor a identificador de jinete `i`;
            incremento `k` en `1`:
            1. Si tiempo asignado a jinete con identificador `k` en carrera `j`
               es igual a tiempo aleatorio `t`:
               1. Asigno verdadero a `encontrado`.
               1. Finalizo búsqueda de tiempo repetido.
      1. Mientras `encontrado` sea falso.
      1. Asigno a jinete con identificador `i` en carrera `j` el tiempo
         aleatorio `t`.

## Función: `imprimirCarreras`

Encuentra los 3 tiempos menores que conforman el podio de cada carrera e
imprima tabla conteniendo número de cada carrera y nombres y tiempos de los
jinetes en las 3 primeras posiciones.

### Entradas:

- `part`: arreglo con datos de jinetes.
- `pods`: matriz con carrera por fila y puesto de jinete por columna.
- `jins`: cantidad de jinetes.
- `cars`: cantidad de carreras.

### Salidas

- Ninguna.

### Pseudo-código

1. Imprimo encabezado de tabla.
1. Para `c` desde `0`; hasta `c` menor a cantidad de carreras `cars`;
   incremento `c` en `1`:
   1. Declaro arreglo auxiliar `idJinPodio` de tamaño igual a cantidad de
      jinetes `JINETES`.
   1. Inicializo arreglo `idJinPodio` para que cada posición coincida con
      identificador de jinete.
   1. Para `i` desde `0`; hasta `i` menor a `3` (ordeno por método de selección
      solo 3 mínimos); incremento `i` en 1:
      1. Declaro variable `posMin` para almacenar la posición del jinete con
         mínimo tiempo y asigno el valor del índice `i`.
      1. Para `j` desde `i + 1`; hasta `j` menor a cantidad de jinetes `jins`;
         incremento `j` en `1`:
         1. Si el tiempo en la carrera `c` del jinete con identificador en la
            posición `j` del arreglo auxiliar `idJinPodio` es menor al tiempo
            en la carrera `c` del jinete con identificador `posMin` del arreglo
            auxiliar `idJinPodio`:
            1. Asigno a `posMin` el valor de `j`.
      1. Si se ha encontrado un nuevo mínimo (`posMin` es diferente a `i`):
         1. Intercambio entre `posMin` e `i` en arreglo auxiliar `idJinPodio`. 
  1. Asigno 3 identificadores de jinetes en la fila de la carrera `c` de la
     matriz de podios `pods`.
  1. Imprimo en pantalla una línea de tabla con resultados.


## Función: `ganador`

Retorna nombre del jinete que ganó más carreras.

### Entradas

- `part`: arreglo con datos de jinetes.
- `pods`: matriz con carrera por fila y puesto de jinete por columna.
- `jins`: cantidad de jinetes.
- `cars`: cantidad de carreras.

### Salida

- Nombre del jinete ganador. 

### Pseudo-código

1. Declaro arreglo de `victorias` con tamaño igual a cantidad de jinetes e
   inicializado en ceros. 
1. Para `i` igual a `0`; hasta `i` menor que cantidad de carreras `cars`;
   incremento `i` en `1`:
   1. Sumo 1 a `victorias` en posición almacenada en la posición de la carrera
      `i` (fila) y el primer lugar (columna `0`).
1. Declaro variable `maxVic` para almacenar máxima cantidad de victorias de un
   jinete e inicializo a `0`.
1. Declaro variable `idGan` para almacenar identificador de jinete ganador.
1. Para  `i` igual a 0; hasta `i` menor que cantidad de jinetes `jins`;
   incremento `i` en `1`:
   1. Si cantidad de victorias del jinete `victorias[i]` es mayor a máxima
      cantidad de victorias `maxVic`:
      1. Asigno cantidad de victorias del jinete `victorias[i]` a máxima
         cantidad de victorias `maxVic`.
      1. Asigno posición de elemento actual `i` al identificador de jinete
         ganador `idGan`.
1. Retorno nombre con identificador de jinete ganador `part[idGan].nombre`.

## Función: `calcularPuntos`

Calcula puntaje por jinete a partir de resultado de carreras.

### Entradas

- `part`: arreglo con datos de jinetes.
- `pods`: matriz con carrera por fila y puesto de jinete por columna.
- `jins`: cantidad de jinetes.
- `cars`: cantidad de carreras.

### Salidas

- Ninguna.

### Pseudo-código

1. Para `i` desde `0`; hasta `i` menor a cantidad de jinetes `jins`; incremento
   `i` en `1`:
   1. Asigno al miembro `puntos` del jinete con posición `i` el valor `0`.
1. Para `i` desde `0`; hasta `i` menor a cantidad de carreras `cars`;
   incremento `i` en `1`:
   1. Incremento `5` puntos a miembro `puntos` del jinete con posición
      almacenada en la posición carrera `i` (fila) y columna `0` (primer lugar
      en podio) del arreglo de podios `pods`.
   1. Si diferencia entre tiempo de segundo `part[pods[i][1]].tiempos[i]` y
      primer jinete `part[pods[i][0]].tiempos[i]` es mayor a `5`:
      1. Incremento `1` a miembro `puntos` del jinete con posición almacenada
          en la posición carrera `i` (fila) y columna `0` (primer lugar en
          podio) del arreglo de podios `pods`.
   1. Incremento `3` a miembro `puntos` del jinete con posición almacenada en
      la posición carrera `i` (fila) y columna `1` (segundo lugar en podio) del
      arreglo de podios `pods`.
   1. Incremento `1` a miembro `puntos` del jinete con posición almacenada en
      la posición carrera `i` (fila) y columna `2` (tercer lugar en podio) del
      arreglo de podios `pods`.

## Función: `reportarTorneo`

Imprime en pantalla tabla descendente de posiciones con nombres de jinetes y
puntos obtenidos durante torneo.

### Entradas

- `part`: arreglo con datos de jinetes.
- `jins`: cantidad de jinetes.

### Salidas

- Ninguna.

### Pseudo-código

1. Declaro arreglo para almacenar identificadores de jinetes `idJin`.
1. Para `i` desde `0`; hasta `i` menor a cantidad de jinetes `jins`; incremento
   `i` en `1`:
   1. Asigno valor de `i` como identificador de jinete `idJin[i]`
1. Para `i` desde `0`; hasta `i` menor a cantidad de jinetes `jins` menos `1`;
   incremento `i` en `1`:
   1. Creo variable para almacenar posición de máximo `posMax` y asigno
      posición actual `i`.
   1. Para `j` desde `i + 1`; hasta `j` menor a cantidad de jinetes `jins`;
      incremento `j` en `1`:
      1. Si miembro `puntos` en jinete con posición `idJin[j]` es major a
         miembro `puntos` en jinete con posición `idJin[maxPos]`:
         1. Asigno a posición de maximo encontrado `posMax` el valor de `j`.
   1. Si posición de máximo encontrado `maxPos` es diferente a posición actual
      `i` (encontré un nuevo máximo de puntos):
      1. Creo variable para almacenar temporalmente identificador `auxId` y
         asigno valor de identificador actual `idJin[i]`.
      1. Asigno a posición de identificador actual `idJin[i]` el identificador
         máximo encontrado `idJin[posMax]`. 
      1. Asigno el valor temporal de `auxId` a la posición donde se encontraba
         el valor máximo `idJin[posMax]`.
1. Imprimo encabezado de tabla de posiciones del torneo con nombres y puntajes.
1. Para `i` desde `0`; hasta `i` menor a cantidad de jinetes `jins`; incremento
   `i` en `1`:
   1. Imprimo en una línea posición `i`, nombre de jinete
      `part[idJin[i]].nombre` y puntaje `part[idJin[i]].puntos`.
